20 Nov 2024

#### Bug fixes

- Fix that the new tiers resource was omitted from etcd->Kubernetes migration. [calico 9494](https://github.com/projectcalico/calico/pull/9494) (@fasaxc)
- Fix a panic in Felix when accessing a nil address in flushing host addresses, i.e. flushHostIPUpdates function. [calico 9488](https://github.com/projectcalico/calico/pull/9488) (@mazdakn)
- Add permission to Calico API server to create tier resources. [calico 9484](https://github.com/projectcalico/calico/pull/9484) (@mazdakn)
- Do not fail data store initialisation when unauthorised error happen while creating default and adminnetworkpolicy tiers. Those tiers eventually get created by another component. [calico 9454](https://github.com/projectcalico/calico/pull/9454) (@mazdakn)
- Helm: Fix that uninstall Job had duplicate k8s-app labels [calico 9439](https://github.com/projectcalico/calico/pull/9439) (@caseydavenport)
- Fix missing routes when vxlan mode is cross-subnet and the environment is purely V6 (no V4 host addresses) [calico 9430](https://github.com/projectcalico/calico/pull/9430) (@tomastigera)

#### Other changes

- ebpf: real IPs in bpf debug output with co-re enabled kernels [calico 9465](https://github.com/projectcalico/calico/pull/9465) (@tomastigera)
- Add setting to enable wireguard NAPI threading [calico 9414](https://github.com/projectcalico/calico/pull/9414) (@jrcichra)
